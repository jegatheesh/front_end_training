var Lists = [{id: "my-day",
              name: "My Day",
              todos: []},
             {id: "to-do",
              name: "To Do",
              todos: []}];
var activeId = "my-day";
var activeList = Lists[0];
var activeTodo = {};
var activeSort;

//var UntitledCount = 0;

   var sidebar;
   document.addEventListener('DOMContentLoaded', function() {
     activeSort = sortReverseByName();
   document.getElementById("todo-title").disabled = true;
   sidebar = document.getElementById("sidebar");
   document.getElementById("note-delete-icon").addEventListener("click", deleteActiveTodo);
   document.getElementById("note-div-closer").addEventListener("click", closeNoteDiv);
   document.getElementById("task-type-space").addEventListener("keyup", checkEnter);
   document.getElementById("note-type-space").addEventListener("change", storeNotes);
   document.getElementById("to-do").addEventListener("click",  openList);
   document.getElementById("my-day").addEventListener("click",  openList);
   document.getElementById("todo-title").addEventListener("blur", changeCorrespondingName);
   document.getElementById("todo-title").addEventListener("keyup", checkEnter);
   document.getElementById("list-adder").addEventListener("click", addList);
   document.getElementById("todo-type-space").addEventListener("keyup", checkEnter);
   document.getElementById("sort-button").addEventListener("click", showSortOptions);
   document.getElementById("sort-options").addEventListener("blur", hideSortOptions);
   document.getElementById("date-descend").addEventListener("click", sortReverseByDate);
   document.getElementById("date-ascend").addEventListener("click", sortForwardByDate);
   document.getElementById("name-descend").addEventListener("click", sortReverseByName);
   document.getElementById("name-ascend").addEventListener("click", sortForwardByName);
   });
   function celebrityID () {
     var i = 9;
    this.celebrityIiD = function() {
      return i;
    };
      var getID = function ()  {
          return celebrityID;
        }
      this.setID = function ()  {
            i = 8;
        }
      }
        var mjID = new celebrityID (); // At this juncture, the celebrityID outer function has returned.​
console.log("================================");
mjID.setID();
console.log(mjID.celebrityIiD());
// console.log(typeof mjID);
// console.log(mjID.getID());​
// console.log(mjID.setID(567));​
// console.log(mjID.getID());
console.log("================================");


function addList() {
   document.getElementById("todo-title").disabled = false;
   var id = "list-"+new Date().valueOf().toString();
   var name = "Untitled list " + getUntitledListNumber();
   var title = document.getElementById("todo-title");
   title.value = name;
   title.focus();
   var li = createElement({name: "li",
                           id: id,
                           className: "active",
                           });
   var i = createElement({name: "i",
                          className: "mega-octicon octicon-list-unordered sidebar-icon"
                          });
   var deleteIcon = createElement({name: "i",
                                   id: "delete-icon",
                                   className: "fa fa-times"});
   document.getElementsByClassName("active")[0].classList.remove("active");
   li.appendChild(i);
   li.appendChild(document.createTextNode(name));
   li.appendChild(deleteIcon);
   deleteIcon.addEventListener("click", deleteItem);
   li.addEventListener("click", openList);
   document.getElementById("sidebar-ul").appendChild(li);
   activeList = {id: id,
               name: name,
               todos: []};
   Lists.push(activeList);
   activeId = id;
   removeAllTodoDiv();
}

function changeCorrespondingName() {
      if (document.getElementById("todo-title").value != "") {
          var activeLi = document.getElementById(activeId);
          var validName = getValidName(document.getElementById("todo-title").value);
          activeLi.childNodes[1].nodeValue = validName;
          activeList.name = validName;
          document.getElementById("todo-title").value = validName;
      } else {
          document.getElementById("todo-title").value = activeList.name;
      }
  }

var openList = function() {
    if (event.target.id == "") {
        activeId = event.target.parentNode.id;
    } else {
        activeId = event.target.id;
    }
    activeList = Lists.filter(function (list) {
        return list.id == activeId ;})[0];
    document.getElementById("todo-title").value = activeList.name;
    document.getElementsByClassName("active")[0].classList.remove("active");
    document.getElementById(activeList.id).className += " active";
    removeAllTodoDiv();
    if (activeList.todos.length != 0) {
        addCurrentListTodos();
    }
    if (activeId == "my-day" || activeId == "to-do") {
        document.getElementById("todo-title").disabled = true;
    } else {
        document.getElementById("todo-title").disabled = false;
    }
}

var addTodoToLists = function() {
    var date = new Date();
    var todoId = "todo-" + date.valueOf().toString();
    if (document.getElementById("todo-type-space").value != "") {
        // var todoDiv = createTodoDiv(todo.id, todo.name);
        var todo = { id: todoId,
                     name: document.getElementById("todo-type-space").value,
                     notes: [],
                     isChecked: false,
                     task: "",
                     date: date
                   };
        activeList.todos.push(todo);
        activeSort;
        document.getElementById(activeList.id).click();
      //  document.getElementById("todo-list").appendChild(createTodoDiv(todo.id));
    }
    var parentNode = event.target.parentNode;
    document.getElementById("todo-type-space").value = "";
}

var createTodoDiv = function(id) {
   var todo = getTodoById(id);
   var div = createElement({name: "div"});
   var i =createElement({name: "i"});
   i.style.cursor = "pointer";
   var deleteIcon = createElement({name: "i",
                                   className: "fa fa-times",
                                   id: "delete-icon"});
   if (todo.isChecked) {
       i.className = "fa fa-check-circle margin-right-5";
       i.style.color = "#56ba0f";
   } else {
       i.className = "fa fa-circle-thin margin-right-5";
       i.style.color = "black";
    }
   addCheckBoxEventstoElement(i, todo);
   div.id = id;
   div.appendChild(i);
   div.appendChild(document.createTextNode(todo.name));
   div.appendChild(deleteIcon);
   if (todo.isChecked) {
     div.style.textDecoration = "line-through";
   }
   if (todo.notes.length != 0) {
       var noteDiv = createNoteIconDiv();
       div.appendChild(noteDiv);
   }
   deleteIcon.addEventListener("click", deleteItem);
   div.addEventListener("dblclick", openTodo);
   return div;
}
var createNoteDiv = function(noteId) {
    var div = createElement({name: "div"});
    var deleteIcon = createElement({name: "i",
                                    className: "fa fa-times cursor-pointer",
                                    id: "delete-icon"});
    var note = getNoteById(noteId);
    div.appendChild(document.createTextNode(note.name));
    div.appendChild(deleteIcon);
    div.id = noteId;
    deleteIcon.addEventListener("click", deleteItem);
    return div;
}

var createNoteIconDiv  = function() {var noteDiv = document.createElement("div");
    var noteIcon = createElement({name: "i",
                                 className: "fa fa-sticky-note",
                                 id: "note-icon"});
    noteIcon.innerHTML = "Notes";
    return noteIcon;
}

var addNote = function() {
    if (document.getElementById("task-type-space").value != "") {
        if (activeTodo.notes.length == 0 ) {
            var noteDiv = createNoteIconDiv();
            document.getElementById(activeTodo.id).appendChild(noteDiv);
        }
        var noteId = "note-" + new Date().valueOf().toString();
        var name = document.getElementById("task-type-space").value;
        var note = {id: noteId,
                    name: name,
                    }
        activeTodo.notes.push(note);
        document.getElementById("note-list").appendChild(createNoteDiv(note.id));
        document.getElementById("task-type-space").value = "";
    }
}

var closeNoteDiv = function() {
    document.getElementById("notes-div").className = "shrink-notes";
    document.getElementById("workspace").className = "expand-workspace";
}

var clearNote = function() {
    document.getElementById("task-type-space").value = "";
}

var openTodo = function() {
    if (event.target.id != "check-box") {
        var notesDiv = document.getElementById("notes-div");
        if (event.target.id != "note-icon") {
            if (event.target.id == "") {
                activeTodo = getTodoById(event.target.parentNode.parentNode.id);
            } else {
                activeTodo = getTodoById(event.target.id);
            }
        } else {
            activeTodo = getTodoById(event.target.parentNode.id);
        }
        removeAllNoteDiv();
        if (activeTodo.notes.length != 0) {
            addCurrentTodoNotes();
        }
        notesDiv.className = "expand-notes";
        document.getElementById("workspace").className = "shrink-workspace";
        document.getElementById("todo-header").innerHTML = activeTodo.name;
        document.getElementById("note-type-space").value = activeTodo.task;
        document.getElementById("task-type-space").value = "";
    }
}

var getCurrentListByName = function(currentListName) {
 return List.filter(function(obj) {
   return obj.name == currentListName;
 });
}

var getTodoById = function(todoId) {
    return activeList.todos.filter(function(todo) {
        return todo.id== todoId;
    })[0];
}

var getNoteById = function(noteId) {
    return activeTodo.notes.filter(function(note) {
        return note.id== noteId;
    })[0];
}

var isChechedTodo = function(todoId) {
    return getTodoById(todoId).isChecked;
}

var removeAllTodoDiv = function() {
   var todoListDiv = document.getElementById("todo-list")
    while (todoListDiv.hasChildNodes()) {
        todoListDiv.removeChild(todoListDiv.lastChild);
    }
}

var addCurrentListTodos = function() {
    var todoListDiv = document.getElementById("todo-list");
    for (var todo in activeList.todos){
        todoListDiv.appendChild(createTodoDiv(activeList.todos[todo].id));
    }
}

var removeAllNoteDiv = function() {
   var noteListDiv = document.getElementById("note-list");
    while (noteListDiv.hasChildNodes()) {
        noteListDiv.removeChild(noteListDiv.lastChild);
    }
}

var addCurrentTodoNotes = function() {
    var todoListDiv = document.getElementById("note-list");
    for (var note in activeTodo.notes){
        todoListDiv.appendChild(createNoteDiv(activeTodo.notes[note].id));
    }
}

var deleteItem = function() {
    event.stopPropagation();
    var id = event.currentTarget.parentNode.id;
    var item = id.split("-");
    if (item[0] == "list") {
        if (id == activeList.id) {
            document.getElementById("my-day").click();
        }
        document.getElementById(id).parentNode.removeChild(document.getElementById(id));
        for (var listIndex in Lists) {
            if (id == Lists[listIndex].id) {
                Lists.splice(listIndex, 1);
                break;
            }
        }
    }
    if (item[0] == "todo") {
        for (var todoIndex in activeList.todos) {
            if (id == activeList.todos[todoIndex].id) {
              if(typeof activeToDo !== "undefined" ) {
              if (id == activeTodo.id) {
                document.getElementById("notes-div").className = "shrink-notes";
                document.getElementById("workspace").className = "expand-workspace";
              }
            }
                activeList.todos.splice(todoIndex, 1);
                document.getElementById(id).parentNode.removeChild(document.getElementById(id));
                break;
            }
        }
    }
    if (item[0] == "note") {
        for (var noteIndex in activeTodo.notes) {
            if (id == activeTodo.notes[noteIndex].id) {
                activeTodo.notes.splice(noteIndex, 1);
                document.getElementById(id).parentNode.removeChild(document.getElementById(id));
                if (activeTodo.notes.length == 0) {
                    document.querySelector('#' + activeTodo.id + ' ' + '#note-icon').remove();
                }
                break;
            }
        }
    }
}

var checkEnter = function() {
    event.preventDefault();
    if (event.keyCode == 13) {
    switch (event.target.id) {
    case "todo-type-space":
        addTodoToLists();
        break;
    case "task-type-space":
        addNote();
        break;
    case "todo-title":
        event.target.blur();
        break;
      }
    }
}

var deleteActiveTodo = function() {
    document.getElementById("notes-div").className = "shrink-notes";
    document.getElementById("workspace").className = "expand-workspace";
        for (var todoIndex in activeList.todos) {
            if (activeTodo.id == activeList.todos[todoIndex].id) {
                activeList.todos.splice(todoIndex, 1);
                document.getElementById(activeTodo.id).parentNode.removeChild(document.getElementById(activeTodo.id));
                break;
            }
        }
}

var storeNotes = function() {
    activeTodo.task = document.getElementById("note-type-space").value;
}

var getUntitledListNumber = function() {
    var n = 1;
    for (var i = 1; i <= Lists.length ; i++) {
      var j = true;
     for (var listIndex in Lists) {
        if (Lists[listIndex].name == "Untitled list " + i.toString()) {
          j = false;
        }
     }
     if (j) {
       n = i;
       break;
     }
   }
   return n ;
}

var getValidName = function(name) {
    var i = 1;
    var validName = name;
    for(var listIndex = 0; listIndex < Lists.length; listIndex++) {
      if (activeList != Lists[listIndex] && Lists[listIndex].name == validName) {
        validName = name + i.toString();
        i++;
        listIndex = 0;
      }
    }
    return validName;
}

var createElement = function(elementObject) {
    var element = document.createElement(elementObject.name);
    if (typeof elementObject.className !== undefined) {
        element.className = elementObject.className;
    }
    if (typeof elementObject.id !== undefined) {
        element.id = elementObject.id;
    }
    return element;
}

var addCheckBoxEventstoElement = function(element, todo) {
  //hover action for checklist
    element.addEventListener("mouseover", function() {
      if (!todo.isChecked) {
          element.className = "fa fa-check-circle-o margin-right-5";
          element.style.color = "grey";
       }
     });

     element.addEventListener("mouseout", function() {
       if (!todo.isChecked) {
           element.className = "fa fa-circle-thin margin-right-5";
           element.style.color = "black";
        }
      });

   element.addEventListener("click", function() {
     if (!isChechedTodo(event.target.parentNode.id)) {
       getTodoById(event.target.parentNode.id).isChecked = true;
       element.className = "fa fa-check-circle margin-right-5";
       element.style.color = "#56ba0f";
       element.id = "check-box";
       document.getElementById(event.target.parentNode.id).style.textDecoration = "line-through";
     } else {
       getTodoById(event.target.parentNode.id).isChecked = false;
       element.className = "fa fa-circle-thin margin-right-5";
       element.style.color = "black";
       document.getElementById(event.target.parentNode.id).style.textDecoration = "none";
     }
   });
}

var sortForwardByDate = function() {
    activeList.todos.sort(function(a, b) {
      return a.date - b.date;
    });
    document.getElementById(activeList.id).click();
}

var sortReverseByDate = function() {
    activeList.todos.sort(function(a, b) {
      return b.date - a.date;
    });
    document.getElementById(activeList.id).click();
}

var sortForwardByName = function() {
    activeList.todos.sort(function(a, b) {
    if(a.name < b.name) return -1;
    if(a.name > b.name) return 1;
    return 0;
    });
    document.getElementById(activeList.id).click();
}

var sortReverseByName = function() {
    activeList.todos.sort(function(a, b) {
    if(a.name > b.name) return -1;
    if(a.name < b.name) return 1;
    return 0;
    });
    document.getElementById(activeList.id).click();
}

var showSortOptions = function() {
    document.getElementById("sort-options").style.display = "block";
    document.getElementById("sort-options").focus();
}

var hideSortOptions = function() {
    document.getElementById("sort-options").style.display = "none";
}
